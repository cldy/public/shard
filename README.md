# Cloudey Shard

**Shard is a framework and platform for developing robust, enterprise-grade web applications and APIs.** 

Shard features:
- All the great features of NestJS which Shard is built on
- Modular, loosely coupled architecture for the entire application
- Automatic CRUD REST API controllers with minimal code
- Automatic Swagger/OpenAPI documentation
- Automatic CQRS for all models
- JWT and password authentication (Passport)
- Advanced **field-level** role, subject, and attribute based authorisation with database-persisted rules, and automatic filtering for outgoing data
- Automatic validation and transformation of requests into DTOs
- Fast and highly flexible serialization and filtering of responses (using Marshal.ts)
- Robust event handling framework
- Automatic security headers following best practices
- Request throttling
- Redis or in-memory cache for all database queries
- Highly asynchronous request handling
- Type-safe environment configuration
- Easy deployment using Docker and docker-compose

Planned features:
- Database-persisted settings management
- Centralised logging integration (Winston, Sentry)
- Event/Pub-Sub queues using Redis
- Scheduler for cron functionality
- Auditing for all events and CQRS actions
- Bulk C*UD actions support for automatic CRUD REST API
- Much more!

**Shard is currently NOT licensed for third-party use, distribution or reproduction.**

---

THIS SOFTWARE IS PROPRIETARY. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained from Cloudey IT Ltd.

Copyright (C) 2020 - Cloudey IT Ltd. All rights reserved. Cloudey(R) is a registered trademark (58134) owned by Cloudey IT Ltd.  

