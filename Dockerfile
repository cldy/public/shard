FROM node:14-alpine as development

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN apk --no-cache --virtual build-dependencies add python make g++

RUN yarn install --only=development

RUN apk del build-dependencies

COPY . .

RUN yarn run build

FROM node:14-alpine as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN apk --no-cache --virtual build-dependencies add python make g++

RUN yarn global add node-gyp
RUN yarn install --only=production

RUN apk del build-dependencies

COPY . .

COPY --from=development /usr/src/app/dist ./dist

CMD ["yarn", "start:prod"]