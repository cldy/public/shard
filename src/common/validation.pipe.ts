import { ArgumentMetadata, BadRequestException, Injectable, Optional, PipeTransform, Scope, Type } from '@nestjs/common'
import { plainToClass, validate } from '@marcj/marshal'

// Transforms and validates request parameters
@Injectable({scope: Scope.TRANSIENT})
export class ValidationPipe implements PipeTransform {
    constructor (
        // The target type for the validator. Overrides reflected metatype. Required when using generics.
        @Optional()
        private target?: Type<any>,
        // Accept arrays containing target type. All elements of the array will be converted to the target type and validated.
        @Optional()
        private array?: boolean,
    ) {
    }

    private static shouldNotValidate (metatype: Type<any>): boolean {
        const types: Type<any>[] = [String, Boolean, Number, Array, Object]
        return types.includes(metatype)
    }

    async transform (value: any, metadata: ArgumentMetadata): Promise<any> {
        const {metatype} = metadata

        this.target = this.target || metatype

        if (!this.target || ValidationPipe.shouldNotValidate(this.target)) {
            return value
        }

        if (Array.isArray(value)) {
            return this.array
                ? await Promise.all(value.map(async item => await this.transform(item, metadata)))
                : value
        }

        const object = plainToClass(this.target, value)
        const errors = validate(this.target, object)

        if (errors.length > 0) {
            throw new BadRequestException(errors, 'Validation error')
        }

        return object
    }
}