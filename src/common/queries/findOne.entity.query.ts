import { IQuery } from '@nestjs/cqrs'
import { BaseModel } from '../base/base.model'
import { Type } from '@nestjs/common'
import { FindOneOptions } from 'typeorm'

export class FindOneEntityQuery<T extends BaseModel> implements IQuery {
    constructor (
        public readonly entity: Type<T>,
        public readonly options?: FindOneOptions<T>,
    ) {
    }
}