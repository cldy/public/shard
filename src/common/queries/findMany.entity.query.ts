import { BaseModel } from '../base/base.model'
import { IQuery } from '@nestjs/cqrs'
import { Type } from '@nestjs/common'
import { FindManyOptions } from 'typeorm'

export class FindManyEntityQuery<T extends BaseModel> implements IQuery {
    constructor (
        public readonly entity: Type<T>,
        public readonly options?: FindManyOptions<T>,
    ) {
    }
}