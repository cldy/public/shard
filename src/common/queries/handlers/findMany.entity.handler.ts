import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { BaseModel } from '../../base/base.model'
import { FindManyEntityQuery } from '../findMany.entity.query'
import { getRepository } from 'typeorm'

@QueryHandler(FindManyEntityQuery)
export class FindManyEntityHandler implements IQueryHandler<FindManyEntityQuery<BaseModel>, BaseModel[]> {
    async execute (query: FindManyEntityQuery<BaseModel>): Promise<BaseModel[]> {
        const repository = getRepository(query.entity)
        return await repository.find(query.options)
    }
}