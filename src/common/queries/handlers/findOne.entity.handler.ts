import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { FindOneEntityQuery } from '../findOne.entity.query'
import { BaseModel } from '../../base/base.model'
import { getRepository } from 'typeorm'

@QueryHandler(FindOneEntityQuery)
export class FindOneEntityHandler implements IQueryHandler<FindOneEntityQuery<BaseModel>, BaseModel> {
    async execute (query: FindOneEntityQuery<BaseModel>): Promise<BaseModel> {
        const repository = getRepository(query.entity)
        return await repository.findOne(query.options)
    }
}