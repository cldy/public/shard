import { ICommand } from '@nestjs/cqrs'
import { BaseModel } from '../base/base.model'
import { Type } from '@nestjs/common'

export class UpdateEntityCmd<T extends BaseModel> implements ICommand {
    constructor (
        public readonly entity: Type<T>,
        public readonly id: string,
        public readonly dto: Partial<T>,
    ) {
    }
}