import { CommandHandler, EventPublisher, ICommandHandler, QueryBus } from '@nestjs/cqrs'
import { BaseModel } from '../../base/base.model'
import { getRepository } from 'typeorm'
import { UpdateEntityCmd } from '../update.entity.cmd'
import { FindOneEntityQuery } from '../../queries/findOne.entity.query'
import { LogService } from '../../../log/log.service'

@CommandHandler(UpdateEntityCmd)
export class UpdateEntityHandler implements ICommandHandler<UpdateEntityCmd<BaseModel>> {
    constructor (
        private queryBus: QueryBus,
        private publisher: EventPublisher,
        private log: LogService,
    ) {
        this.log.setContext('UpdateEntityHandler')
    }

    async execute (command: UpdateEntityCmd<BaseModel>): Promise<void> {
        const entity = this.publisher.mergeObjectContext(
            await this.queryBus.execute(
                new FindOneEntityQuery(command.entity, {where: {id: command.id}}),
            ),
        )

        if (!entity) {
            this.log.error(`Failed to update non-existent entity with ID ${command.id}`)
        }

        const repository = getRepository(command.entity)
        const updatedEntity = repository.merge(entity, command.dto)
        await repository.save(updatedEntity)

        entity.commit()
    }
}