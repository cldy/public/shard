import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs'
import { CreateEntityCmd } from '../create.entity.cmd'
import { BaseModel } from '../../base/base.model'
import { getRepository } from 'typeorm'
import { LogService } from '../../../log/log.service'

@CommandHandler(CreateEntityCmd)
export class CreateEntityHandler implements ICommandHandler<CreateEntityCmd<BaseModel>> {
    constructor (
        private publisher: EventPublisher,
        private log: LogService,
    ) {
        this.log.setContext('CreateEntityHandler')
    }

    async execute (command: CreateEntityCmd<BaseModel>): Promise<void> {
        const repository = getRepository(command.entity)
        const entity = this.publisher.mergeObjectContext(repository.create(command.dto))

        await repository.save(entity)

        entity.commit()
    }
}