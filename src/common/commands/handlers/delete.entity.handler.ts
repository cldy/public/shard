import { CommandHandler, EventPublisher, ICommandHandler, QueryBus } from '@nestjs/cqrs'
import { BaseModel } from '../../base/base.model'
import { getRepository } from 'typeorm'
import { FindOneEntityQuery } from '../../queries/findOne.entity.query'
import { DeleteEntityCmd } from '../delete.entity.cmd'
import { LogService } from '../../../log/log.service'

@CommandHandler(DeleteEntityCmd)
export class DeleteEntityHandler implements ICommandHandler<DeleteEntityCmd<BaseModel>> {
    constructor (
        private queryBus: QueryBus,
        private publisher: EventPublisher,
        private log: LogService,
    ) {
        this.log.setContext('DeleteEntityHandler')
    }

    async execute (command: DeleteEntityCmd<BaseModel>): Promise<void> {
        const entity = this.publisher.mergeObjectContext(
            await this.queryBus.execute(
                new FindOneEntityQuery(command.entity, {where: {id: command.id}}),
            ),
        )

        if (!entity) {
            this.log.warn(`Attempted to delete entity with ID ${command.id} but entity was not found in the database`)
            return
        }

        const repository = getRepository(command.entity)
        await repository.softRemove(entity)

        entity.commit()
    }
}