import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { classToPlain } from '@marcj/marshal'

@Injectable()
export class SerializerInterceptor implements NestInterceptor {
    async intercept (context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
        return next
            .handle()
            .pipe(
                map(async data => this.serialize(await data)),
            )
    }

    serialize (data: any): any {
        if (data === undefined || data === null) {
            return data
        }

        if (Array.isArray(data)) {
            return data.map(item => this.serialize(item))
        }

        if (data.constructor && data.constructor !== Object) {
            return classToPlain(data.constructor, data)
        }

        return data
    }
}