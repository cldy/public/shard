import { Injectable } from '@nestjs/common'
import argon2 from 'argon2'

@Injectable()
export class CommonService {

    // Hash the given string using a strong hash function (argon2id)
    hash = CommonService.hash

    // Hash the given string using a strong hash function (argon2id)
    static async hash (string: string): Promise<string> {
        // This method is static for easier usage from entity classes which do not typically use DI
        try {
            // If the string is not hashed, the following line will throw an exception. Otherwise, we can return the already hashed string as-is.
            argon2.needsRehash(string)
            return string
        } catch {
            return argon2.hash(string, {type: argon2.argon2id})
        }
    }

    // Verify that the given argon2 hash corresponds to the given plaintext
    async verifyHash (hash: string, plain: string): Promise<boolean> {
        return argon2.verify(hash, plain)
    }


}