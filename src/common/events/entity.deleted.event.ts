import { BaseModel } from '../base/base.model'
import { DeletedEvent } from '../base/events/deleted.event'

export class EntityDeletedEvent<T extends BaseModel> extends DeletedEvent<T> {
}