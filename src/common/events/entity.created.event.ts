import { CreatedEvent } from '../base/events/created.event'
import { BaseModel } from '../base/base.model'

export class EntityCreatedEvent<T extends BaseModel> extends CreatedEvent<T> {
}