import { BaseModel } from '../base/base.model'
import { UpdatedEvent } from '../base/events/updated.event'

export class EntityUpdatedEvent<T extends BaseModel> extends UpdatedEvent<T> {
}