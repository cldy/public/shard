import { AggregateRoot, IEvent } from '@nestjs/cqrs'

export class CreatedEvent<T extends AggregateRoot> implements IEvent {
    constructor (
        public readonly entity: T,
    ) {
    }
}