import { AggregateRoot, IEvent } from '@nestjs/cqrs'

export class UpdatedEvent<T extends AggregateRoot> implements IEvent {
    constructor (
        public readonly entity: T,
    ) {
    }
}