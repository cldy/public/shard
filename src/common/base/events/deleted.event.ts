import { AggregateRoot, IEvent } from '@nestjs/cqrs'

export class DeletedEvent<T extends AggregateRoot> implements IEvent {
    constructor (
        public readonly entity: T,
    ) {
    }
}