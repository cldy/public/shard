import { AggregateRoot } from '@nestjs/cqrs'
import {
    AfterInsert,
    AfterRemove,
    AfterUpdate,
    BeforeInsert,
    CreateDateColumn,
    DeleteDateColumn,
    PrimaryColumn,
    UpdateDateColumn,
    VersionColumn,
} from 'typeorm'
import { ulid } from 'ulid'
import { ApiResponseProperty } from '@nestjs/swagger'
import { EntityCreatedEvent } from '../events/entity.created.event'
import { EntityUpdatedEvent } from '../events/entity.updated.event'
import { EntityDeletedEvent } from '../events/entity.deleted.event'
import { f } from '@marcj/marshal'
import { IsReadOnly } from '../validators/isReadOnly.validator'

export abstract class BaseModel extends AggregateRoot {
    @f.primary().optional().validator(IsReadOnly)
    @ApiResponseProperty()
    @PrimaryColumn()
    id: string

    @f.optional().validator(IsReadOnly)
    @ApiResponseProperty()
    @CreateDateColumn()
    createdAt: string

    @f.optional().validator(IsReadOnly)
    @ApiResponseProperty()
    @UpdateDateColumn()
    updatedAt: string

    @f.optional().validator(IsReadOnly)
    @ApiResponseProperty()
    @DeleteDateColumn()
    deletedAt?: string

    @f.optional().validator(IsReadOnly)
    @ApiResponseProperty()
    @VersionColumn()
    revision: number

    @BeforeInsert()
    private generateId (): void {
        this.id = ulid()
    }

    @AfterInsert()
    private entityCreated (): void {
        this.apply(new EntityCreatedEvent(this))
    }

    @AfterUpdate()
    private entityUpdated (): void {
        this.apply(new EntityUpdatedEvent(this))
    }

    // TODO: Make sure a removal/deletion event also fires on soft-deletes
    @AfterRemove()
    private entityDeleted (): void {
        this.apply(new EntityDeletedEvent(this))
    }
}
