import { LogService } from '../../log/log.service'
import { Injectable, Type } from '@nestjs/common'
import { CommandBus, QueryBus } from '@nestjs/cqrs'
import { CreateEntityCmd } from '../commands/create.entity.cmd'
import { BaseModel } from './base.model'
import { UpdateEntityCmd } from '../commands/update.entity.cmd'
import { DeleteEntityCmd } from '../commands/delete.entity.cmd'
import { FindOneEntityQuery } from '../queries/findOne.entity.query'
import { FindManyOptions, FindOneOptions } from 'typeorm'
import { FindManyEntityQuery } from '../queries/findMany.entity.query'

export interface IBaseService<T extends BaseModel> {
    create (dto: Partial<T>): Promise<void>

    update (id: string, dto: Partial<T>): Promise<void>

    delete (id: string): Promise<void>,

    findOne (options?: FindOneOptions<T>): Promise<T>

    findMany (options?: FindManyOptions<T>): Promise<T[]>
}

export function BaseService<T extends BaseModel> (entity: Type<T>): Type<IBaseService<T>> {
    @Injectable()
    class BaseServiceHost implements IBaseService<T> {
        constructor (
            protected log: LogService,
            protected queryBus: QueryBus,
            protected commandBus: CommandBus,
        ) {
        }

        async onModuleInit (): Promise<void> {
            this.log.setContext(this.constructor.name)
        }

        async create (dto: Partial<T>): Promise<void> {
            await this.commandBus.execute(
                new CreateEntityCmd<T>(entity, dto),
            )
        }

        async update (id: string, dto: Partial<T>): Promise<void> {
            await this.commandBus.execute(
                new UpdateEntityCmd(entity, id, dto),
            )
        }

        async delete (id: string): Promise<void> {
            await this.commandBus.execute(
                new DeleteEntityCmd(entity, id),
            )
        }

        async findOne (options?: FindOneOptions<T>): Promise<T> {
            return await this.queryBus.execute(
                new FindOneEntityQuery(entity, options),
            )
        }

        async findMany (options?: FindManyOptions<T>): Promise<T[]> {
            return await this.queryBus.execute(
                new FindManyEntityQuery(entity, options),
            )
        }
    }

    return BaseServiceHost
}