import {
    Body,
    DefaultValuePipe,
    Delete,
    Get,
    Inject,
    Injectable,
    NotFoundException,
    Param,
    ParseArrayPipe,
    ParseBoolPipe,
    Patch,
    Post,
    Query,
    Type,
} from '@nestjs/common'
import { BaseModel } from './base.model'
import { CommandBus, QueryBus } from '@nestjs/cqrs'
import { FindOneEntityQuery } from '../queries/findOne.entity.query'
import { FindOneOptions } from 'typeorm/find-options/FindOneOptions'
import { FindManyOptions, FindOperator, FindOperatorType } from 'typeorm'
import { ValidationPipe } from '../validation.pipe'
import merge from 'lodash/merge'
import { Claim } from '../../access/claim.model'
import { Access } from '../../access/decorators/access.decorator'
import { Action } from '../../access/action.enum'
import { IBaseService } from './base.service'

export interface CrudControllerOptions {
    dto?: {
        create?: any,
        update?: any,
    }
}

export function CrudController<T extends BaseModel, S extends IBaseService<T>> (entity: Type<T>, service: Type<S>, options?: CrudControllerOptions): any {
    const _options = merge({
        dto: {
            create: entity,
            update: entity,
        },
    }, options)

    @Injectable()
    class CrudControllerHost {
        constructor (
            protected commandBus: CommandBus,
            protected queryBus: QueryBus,
            @Inject(service)
            protected service: S,
        ) {
        }

        @Get(':id')
        @Access(new Claim<T>(Action.READ, entity))
        async findOneById (
            @Param('id') id: string,
            @Query('fields', new DefaultValuePipe([]), ParseArrayPipe) fields: (keyof T)[],
            @Query('with', new DefaultValuePipe([]), ParseArrayPipe) relations: string[],
        ): Promise<T> {
            const options = {
                where: {id},
                relations,
            } as FindOneOptions<T>

            if (fields.length > 0) {
                options.select = ['id', ...fields]
            }

            const result = await this.service.findOne(options)

            if (!result) {
                throw new NotFoundException()
            }

            return result
        }

        @Get()
        @Access(new Claim<T>(Action.LIST, entity))
        async findMany (
            @Query('fields', new DefaultValuePipe([]), ParseArrayPipe) fields: (keyof T)[],
            @Query('with', new DefaultValuePipe([]), ParseArrayPipe) relations: string[],
            @Query('sort', new DefaultValuePipe([]), ParseArrayPipe) sort: [keyof T, ('asc' | 'desc')?] | [],
            @Query('deleted', new DefaultValuePipe(false), ParseBoolPipe) withDeleted: boolean,
            @Query('where', new DefaultValuePipe([]), ParseArrayPipe) where: [keyof T, string | number, FindOperatorType?] | [],
        ): Promise<T[]> {
            const options = {
                relations: relations,
                withDeleted,
            } as FindManyOptions<T>

            if (fields.length > 0) {
                options.select = ['id', ...fields]
            }

            if (where.length > 1) {
                options.where = {
                    [where[0]]: new FindOperator(where[2] ?? 'equal', where[1]),
                }
            }

            if (sort.length > 0) {
                options.order = {
                    [sort[0]]: sort[1]?.toUpperCase() ?? 'ASC',
                } as {
                    // TypeORM types are broken
                    [P in keyof T]: 'ASC' | 'DESC'
                }
            }

            return this.service.findMany(options)
        }

        @Post()
        @Access(new Claim<T>(Action.CREATE, entity))
        async create (
            @Body(new ValidationPipe(_options.dto.create)) dto: Partial<T>,
        ): Promise<void> {
            return this.service.create(dto)
        }

        @Patch(':id')
        @Access(new Claim<T>(Action.UPDATE, entity))
        async update (
            @Param('id') id: string,
            @Body(new ValidationPipe(_options.dto.update)) dto: Partial<T>,
        ): Promise<void> {
            const result = await this.queryBus.execute(new FindOneEntityQuery(entity, {where: {id}}))

            if (!result) {
                throw new NotFoundException()
            }

            return this.service.update(id, dto)
        }

        @Delete(':id')
        @Access(new Claim<T>(Action.DELETE, entity))
        async delete (
            @Param('id') id: string,
        ): Promise<void> {
            const result = await this.queryBus.execute(new FindOneEntityQuery(entity, {where: {id}}))

            if (!result) {
                throw new NotFoundException()
            }

            return this.service.delete(id)
        }
    }

    return CrudControllerHost
}