import { ICommand } from '@nestjs/cqrs'
import { DeepPartial } from 'typeorm'
import { BaseModel } from '../base.model'

export abstract class UpdateCmd<T extends BaseModel> implements ICommand {
    constructor (
        public readonly id: string,
        public readonly dto: DeepPartial<T>,
    ) {
    }
}