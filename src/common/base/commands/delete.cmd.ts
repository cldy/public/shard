import { ICommand } from '@nestjs/cqrs'
import { BaseModel } from '../base.model'

export abstract class DeleteCmd<T extends BaseModel> implements ICommand {
    constructor (
        public readonly id: string,
    ) {
    }
}