import { EventPublisher, ICommandHandler } from '@nestjs/cqrs'
import { Repository } from 'typeorm'
import { Inject } from '@nestjs/common'
import { DeleteCmd } from '../delete.cmd'
import { LogService } from '../../../../log/log.service'
import { BaseModel } from '../../base.model'


export abstract class DeleteHandler<C extends DeleteCmd<any>> implements ICommandHandler<C> {
    @Inject(EventPublisher)
    private readonly publisher: EventPublisher
    @Inject(LogService)
    private readonly log: LogService

    protected constructor (
        protected repository: Repository<BaseModel>,
    ) {
    }

    async execute (command: C): Promise<void> {
        const {id} = command
        const entity = this.publisher.mergeObjectContext(await this.repository.findOne(id))

        if (!entity) {
            this.log.warn(`Attempted to delete entity with ID ${id} but entity was not found in the database`)
            return
        }

        let deletedEntity

        if (entity.hasOwnProperty('deletedAt')) {
            deletedEntity = await this.repository.softRemove(entity)
        } else {
            deletedEntity = await this.repository.remove(entity)
        }

        deletedEntity.commit()
    }
}