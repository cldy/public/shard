import { EventPublisher, ICommandHandler } from '@nestjs/cqrs'
import { CreateCmd } from '../create.cmd'
import { Repository } from 'typeorm'
import { Inject } from '@nestjs/common'
import { BaseModel } from '../../base.model'

export abstract class CreateHandler<C extends CreateCmd<any>> implements ICommandHandler<C> {
    @Inject(EventPublisher)
    private readonly publisher: EventPublisher

    protected constructor (
        protected repository: Repository<BaseModel>,
    ) {
    }

    async execute (command: C): Promise<void> {
        const {dto} = command
        const entity = this.publisher.mergeObjectContext(this.repository.create(dto))
        await this.repository.save(entity)
        entity.commit()
    }
}