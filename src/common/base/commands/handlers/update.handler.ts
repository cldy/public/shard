import { EventPublisher, ICommandHandler } from '@nestjs/cqrs'
import { Repository } from 'typeorm'
import { Inject } from '@nestjs/common'
import { UpdateCmd } from '../update.cmd'
import { BaseModel } from '../../base.model'


export abstract class UpdateHandler<C extends UpdateCmd<any>> implements ICommandHandler<C> {
    @Inject(EventPublisher)
    private readonly publisher: EventPublisher

    protected constructor (
        protected repository: Repository<BaseModel>,
    ) {
    }

    async execute (command: C): Promise<void> {
        const {id, dto} = command
        const entity = await this.repository.findOne(id)
        const updatedEntity = this.publisher.mergeObjectContext(this.repository.merge(entity, dto))
        await this.repository.save(updatedEntity)
        updatedEntity.commit()
    }
}