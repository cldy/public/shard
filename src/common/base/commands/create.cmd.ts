import { ICommand } from '@nestjs/cqrs'
import { DeepPartial } from 'typeorm'
import { BaseModel } from '../base.model'

export abstract class CreateCmd<T extends BaseModel> implements ICommand {
    constructor (
        public readonly dto: DeepPartial<T>,
    ) {
    }
}