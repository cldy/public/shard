import { PropertyValidator, PropertyValidatorError } from '@marcj/marshal'

export class IsReadOnly implements PropertyValidator {
    validate (value: any) {
        if (value !== null) {
            return new PropertyValidatorError('readonly', 'Value is read-only')
        }
    }
}