import { PropertyCompilerSchema, PropertyValidator, PropertyValidatorError } from '@marcj/marshal'

export class IsLowercase implements PropertyValidator {
    validate (value: string, schema: PropertyCompilerSchema) {
        if (value.toLowerCase() !== value) {
            return new PropertyValidatorError('lowercase', `${schema.name} must be lowercase`)
        }
    }
}