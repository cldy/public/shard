import { Global, Module } from '@nestjs/common'
import { CommonService } from './common.service'
import { FindOneEntityHandler } from './queries/handlers/findOne.entity.handler'
import { FindManyEntityHandler } from './queries/handlers/findMany.entity.handler'
import { CreateEntityHandler } from './commands/handlers/create.entity.handler'
import { UpdateEntityHandler } from './commands/handlers/update.entity.handler'
import { DeleteEntityHandler } from './commands/handlers/delete.entity.handler'
import { CqrsModule } from '@nestjs/cqrs'
import { ValidationPipe } from './validation.pipe'
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core'
import { SerializerInterceptor } from './serializer.interceptor'

@Global()
@Module({
    imports: [
        CqrsModule,
    ],
    providers: [
        CommonService,
        FindOneEntityHandler,
        FindManyEntityHandler,
        CreateEntityHandler,
        UpdateEntityHandler,
        DeleteEntityHandler,
        {
            provide: APP_PIPE,
            useClass: ValidationPipe,
        },
        {
            provide: APP_INTERCEPTOR,
            useClass: SerializerInterceptor,
        },
    ],
    exports: [
        CommonService,
    ],
})
export class CommonModule {
}