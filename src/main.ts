import { NestFactory } from '@nestjs/core'
import helmet from 'helmet'
import rateLimit from 'express-rate-limit'
import { NestExpressApplication } from '@nestjs/platform-express'
import { LogService } from './log/log.service'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { AppModule } from './app/app.module'

async function bootstrap () {
    const app = await NestFactory.create<NestExpressApplication>(AppModule)

    // Security
    app.set('trust proxy', 1) // Trust headers set by nginx or other reverse proxy
    app.use(helmet()) // Security hardening middleware
    app.use(
        rateLimit({
            windowMs: 60 * 1000, // 1 minute
            max: 100,
        }),
    )
    // TODO: Add CORS handling

    // Logging
    app.set('logger', false) // Disable default logger
    app.useLogger(await app.resolve(LogService)) // Use logger from LogModule

    // OpenAPI/Swagger documentation
    const swaggerOptions = new DocumentBuilder()
        .setTitle('Cloudey Shard')
        .setDescription('A platform for great things.')
        .setVersion(process.env.npm_package_version ?? 'unknown')
        .addTag('cloudey')
        .build()
    const document = SwaggerModule.createDocument(app, swaggerOptions)
    SwaggerModule.setup('api', app, document)

    // Let's go!
    await app.listen(3000)
}

// noinspection JSIgnoredPromiseFromCall
bootstrap()
