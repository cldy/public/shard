import { forwardRef, Module } from '@nestjs/common'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { authConfig } from './config/auth.config'
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { UserModule } from '../user/user.module'
import { AuthService } from './auth.service'
import { LocalStrategy } from './local.strategy'
import { JwtStrategy } from './jwt.strategy'
import { JwtGuard } from './guards/jwt.guard'
import { LocalGuard } from './guards/local.guard'
import { AuthController } from './auth.controller'
import { CqrsModule } from '@nestjs/cqrs'

@Module({
    imports: [
        ConfigModule.forFeature(authConfig),
        PassportModule,
        JwtModule.registerAsync({
            useFactory: (config: ConfigType<typeof authConfig>) => ({
                privateKey: config.privateKey,
                signOptions: {
                    expiresIn: config.jwtExpiresIn,
                    algorithm: 'RS256',
                },
            }),
            imports: [ConfigModule.forFeature(authConfig)],
            inject: [authConfig.KEY],
        }),
        forwardRef(() => UserModule),
        CqrsModule,
    ],
    controllers: [
        AuthController,
    ],
    providers: [
        AuthService,
        LocalStrategy,
        JwtStrategy,
        LocalGuard,
        JwtGuard,
    ],
})
export class AuthModule {
}