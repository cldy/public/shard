import { Body, Controller, Post } from '@nestjs/common'
import { AuthService } from './auth.service'
import { TokenDto } from './dto/token.dto'
import { LoginDto } from './dto/login.dto'
import { RegisterDto } from './dto/register.dto'

@Controller('auth')
export class AuthController {
    constructor (
        private authService: AuthService,
    ) {
    }

    @Post('login')
    async login (@Body() loginDto: LoginDto): Promise<TokenDto> {
        return this.authService.login(loginDto)
    }

    @Post('register')
    async register (@Body() dto: RegisterDto): Promise<void> {
        return await this.authService.register(dto)
    }
}