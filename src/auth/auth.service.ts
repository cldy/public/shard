import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { User } from '../user/user.entity'
import { TokenDto } from './dto/token.dto'
import { IJwtPayload } from './jwtPayload.interface'
import { LoginDto } from './dto/login.dto'
import { RegisterDto } from './dto/register.dto'
import { CommonService } from '../common/common.service'
import { CommandBus, QueryBus } from '@nestjs/cqrs'
import { CreateUserCmd } from '../user/commands/create.user.cmd'
import { FindOneEntityQuery } from '../common/queries/findOne.entity.query'

@Injectable()
export class AuthService {
    constructor (
        private jwtService: JwtService,
        private commonService: CommonService,
        private commandBus: CommandBus,
        private queryBus: QueryBus,
    ) {
    }

    async validateUser (username: string, password: string): Promise<User> {
        const user = await this.queryBus.execute(new FindOneEntityQuery(User, {where: {username}}))
        if (!user) {
            return null
        }
        if (user.password && await this.commonService.verifyHash(user.password, password)) {
            return user
        }
        return null
    }

    async getTokenForUser (user: User): Promise<TokenDto> {
        const payload: IJwtPayload = {
            username: user.username,
            sub: user.id,
        }

        return new TokenDto(
            await this.jwtService.signAsync(payload),
            user.id,
        )
    }

    async login (dto: LoginDto): Promise<TokenDto> {
        const user = await this.validateUser(dto.username, dto.password)
        if (!user) {
            throw new UnauthorizedException('Invalid username or password')
        }
        // TODO: Add UserLogin event
        return this.getTokenForUser(user)
    }

    async register (dto: RegisterDto): Promise<void> {
        const {username} = dto
        if (await this.queryBus.execute(new FindOneEntityQuery(User, {where: {username}}))) {
            throw new BadRequestException(['username must be unique'])
        }
        // TODO: Replace with dedicated RegisterUser command
        await this.commandBus.execute(new CreateUserCmd(dto))
    }
}