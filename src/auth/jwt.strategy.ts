import { Inject, Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { ConfigType } from '@nestjs/config'
import { authConfig } from './config/auth.config'
import { User } from '../user/user.entity'
import { IJwtPayload } from './jwtPayload.interface'
import { QueryBus } from '@nestjs/cqrs'
import { FindOneEntityQuery } from '../common/queries/findOne.entity.query'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor (
        @Inject(authConfig.KEY)
        private config: ConfigType<typeof authConfig>,
        private queryBus: QueryBus,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: config.publicKey,
        })
    }

    async validate (payload: IJwtPayload): Promise<User> {
        return await this.queryBus.execute(new FindOneEntityQuery(User, {where: {id: payload.sub}}))
    }
}