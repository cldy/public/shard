import { f } from '@marcj/marshal'

export class LoginDto {
    @f username: string
    @f password: string
}