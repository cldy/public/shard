import { f } from '@marcj/marshal'

export class TokenDto {
    @f token: string
    @f userId: string

    constructor (token: string, userId: string) {
        this.token = token
        this.userId = userId
    }
}