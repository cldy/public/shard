import { f } from '@marcj/marshal'
import { IsLowercase } from '../../common/validators/isLowercase.validator'

// TODO: Add missing validation rules
export class RegisterDto {
    @f
    name: string

    @f.validator(IsLowercase)
    username: string

    @f
    email: string

    @f
    password: string
}