import { AfterInsert, AfterRemove, AfterUpdate, BeforeInsert, BeforeUpdate, Column, Entity } from 'typeorm'
import { BaseModel } from '../common/base/base.model'
import { CommonService } from '../common/common.service'
import { UserCreatedEvent } from './events/user.created.event'
import { UserUpdatedEvent } from './events/user.updated.event'
import { UserDeletedEvent } from './events/user.deleted.event'
import { Roles } from '../access/decorators/roles.decorator'
import { Role } from '../access/role.entity'
import { UseAccess } from '../access/decorators/useAccess.decorator'
import { f, forwardRef } from '@marcj/marshal'
import { IsReadOnly } from '../common/validators/isReadOnly.validator'

@UseAccess()
@Entity()
export class User extends BaseModel {

    @f
    @Column()
    name: string

    @f
    @Column({unique: true})
    username: string

    @f
    @Column()
    email: string

    @f.exclude()
    @Column()
    password: string

    @f.array(forwardRef(() => Role)).optional().validator(IsReadOnly)
    @Roles()
    roles: Role[]

    @BeforeInsert()
    @BeforeUpdate()
    async hashPassword (): Promise<void> {
        if (this.password) {
            this.password = await CommonService.hash(this.password)
        }
    }

    @AfterInsert()
    private userCreated (): void {
        this.apply(new UserCreatedEvent(this))
    }

    @AfterUpdate()
    private userUpdated (): void {
        this.apply(new UserUpdatedEvent(this))
    }

    @AfterRemove()
    private userDeleted (): void {
        this.apply(new UserDeletedEvent(this))
    }
}