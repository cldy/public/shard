import { User } from '../user.entity'
import { CreatedEvent } from '../../common/base/events/created.event'

export class UserCreatedEvent extends CreatedEvent<User> {
}