import { User } from '../user.entity'
import { DeletedEvent } from '../../common/base/events/deleted.event'

export class UserDeletedEvent extends DeletedEvent<User> {
}