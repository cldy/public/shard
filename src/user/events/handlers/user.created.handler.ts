import { EventsHandler, IEventHandler } from '@nestjs/cqrs'
import { UserCreatedEvent } from '../user.created.event'
import { LogService } from '../../../log/log.service'

// TODO: This event handler is for debugging/POC purposes only and should eventually be removed
@EventsHandler(UserCreatedEvent)
export class UserCreatedHandler implements IEventHandler<UserCreatedEvent> {
    constructor (
        private log: LogService,
    ) {
    }

    handle (event: UserCreatedEvent): void {
        this.log.log(`User created with ID ${event.entity.id}`)
    }
}