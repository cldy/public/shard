import { User } from '../user.entity'
import { UpdatedEvent } from '../../common/base/events/updated.event'

export class UserUpdatedEvent extends UpdatedEvent<User> {
}