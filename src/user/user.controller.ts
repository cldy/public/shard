import { Controller, UseGuards } from '@nestjs/common'
import { User } from './user.entity'
import { CrudController } from '../common/base/crud.controller'
import { JwtGuard } from '../auth/guards/jwt.guard'
import { UserService } from './user.service'
import { UpdateUserDto } from './dtos/update.user.dto'
import { AccessGuard } from '../access/access.guard'

@Controller('users')
@UseGuards(JwtGuard, AccessGuard)
export class UserController extends CrudController(User, UserService, {
    dto: {
        update: UpdateUserDto,
    },
}) {
}