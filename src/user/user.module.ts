import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from './user.entity'
import { UserController } from './user.controller'
import { UserCreatedHandler } from './events/handlers/user.created.handler'
import { CreateUserHandler } from './commands/handlers/create.user.handler'
import { CqrsModule } from '@nestjs/cqrs'
import { UpdateUserHandler } from './commands/handlers/update.user.handler'
import { DeleteUserHandler } from './commands/handlers/delete.user.handler'
import { FindOneUserHandler } from './queries/handlers/findOne.user.handler'
import { UserService } from './user.service'

@Module({
    imports: [
        TypeOrmModule.forFeature([User]),
        CqrsModule,
    ],
    providers: [
        UserCreatedHandler,
        CreateUserHandler,
        UpdateUserHandler,
        DeleteUserHandler,
        FindOneUserHandler,
        UserService,
    ],
    controllers: [
        UserController,
    ],
})
export class UserModule {
}