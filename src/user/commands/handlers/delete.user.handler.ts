import { InjectRepository } from '@nestjs/typeorm'
import { User } from '../../user.entity'
import { Repository } from 'typeorm'
import { DeleteHandler } from '../../../common/base/commands/handlers/delete.handler'
import { DeleteUserCmd } from '../delete.user.cmd'
import { CommandHandler } from '@nestjs/cqrs'

@CommandHandler(DeleteUserCmd)
export class DeleteUserHandler extends DeleteHandler<DeleteUserCmd> {
    constructor (
        @InjectRepository(User) repository: Repository<User>,
    ) {
        super(repository)
    }
}