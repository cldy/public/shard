import { UpdateHandler } from '../../../common/base/commands/handlers/update.handler'
import { UpdateUserCmd } from '../update.user.cmd'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from '../../user.entity'
import { Repository } from 'typeorm'
import { CommandHandler } from '@nestjs/cqrs'

@CommandHandler(UpdateUserCmd)
export class UpdateUserHandler extends UpdateHandler<UpdateUserCmd> {
    constructor (
        @InjectRepository(User) repository: Repository<User>,
    ) {
        super(repository)
    }
}