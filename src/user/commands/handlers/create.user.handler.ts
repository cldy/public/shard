import { CommandHandler } from '@nestjs/cqrs'
import { CreateUserCmd } from '../create.user.cmd'
import { Repository } from 'typeorm'
import { User } from '../../user.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { CreateHandler } from '../../../common/base/commands/handlers/create.handler'

@CommandHandler(CreateUserCmd)
export class CreateUserHandler extends CreateHandler<CreateUserCmd> {
    constructor (
        @InjectRepository(User) repository: Repository<User>,
    ) {
        super(repository)
    }
}