import { UpdateCmd } from '../../common/base/commands/update.cmd'
import { User } from '../user.entity'

export class UpdateUserCmd extends UpdateCmd<User> {
}