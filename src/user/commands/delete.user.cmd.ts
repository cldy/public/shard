import { DeleteCmd } from '../../common/base/commands/delete.cmd'
import { User } from '../user.entity'

export class DeleteUserCmd extends DeleteCmd<User> {
}