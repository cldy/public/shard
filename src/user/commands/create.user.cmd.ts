import { User } from '../user.entity'
import { CreateCmd } from '../../common/base/commands/create.cmd'

export class CreateUserCmd extends CreateCmd<User> {
}