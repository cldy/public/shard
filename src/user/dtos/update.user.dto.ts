import { f } from '@marcj/marshal'

export class UpdateUserDto {
    @f.optional() name: string
    @f.optional() username: string
    @f.optional() email: string
    @f.optional() password: string
}