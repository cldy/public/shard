import { BadRequestException, Injectable } from '@nestjs/common'
import { BaseService } from '../common/base/base.service'
import { User } from './user.entity'
import { CommandBus, QueryBus } from '@nestjs/cqrs'
import { FindOneEntityQuery } from '../common/queries/findOne.entity.query'
import { CreateEntityCmd } from '../common/commands/create.entity.cmd'
import { LogService } from '../log/log.service'

@Injectable()
export class UserService extends BaseService(User) {
    constructor (
        protected log: LogService,
        protected queryBus: QueryBus,
        protected commandBus: CommandBus,
    ) {
        super()
    }

    async create (dto: Partial<User>): Promise<void> {
        await this.validateUsernameIsUnique(dto.username)

        await this.commandBus.execute(new CreateEntityCmd(User, dto))
    }

    async validateUsernameIsUnique (username: string): Promise<void> {
        const usernameExists = await this.queryBus.execute(new FindOneEntityQuery(User, {where: {username}}))

        if (usernameExists) {
            // TODO: Replace with Exception factory
            throw new BadRequestException({
                statusCode: 400,
                message: [
                    {
                        path: 'username',
                        code: 'unique',
                        message: 'username must be unique',
                    },
                ],
                error: 'Validation error',
            })
        }
    }
}