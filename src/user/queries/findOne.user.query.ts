import { IQuery } from '@nestjs/cqrs'
import { User } from '../user.entity'

export class FindOneUserQuery implements IQuery {
    constructor (
        public readonly id: string,
        public readonly fields?: (keyof User)[],
        public readonly join?: string[],
    ) {
    }
}