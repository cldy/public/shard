import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { FindOneUserQuery } from '../findOne.user.query'
import { User } from '../../user.entity'
import { Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'

@QueryHandler(FindOneUserQuery)
export class FindOneUserHandler implements IQueryHandler<FindOneUserQuery, User> {
    constructor (
        @InjectRepository(User)
        private repository: Repository<User>,
    ) {
    }

    async execute (query: FindOneUserQuery): Promise<User> {
        const {id, fields, join} = query
        console.log('Using User handler!')
        return await this.repository.findOne(id, {
            select: fields,
            relations: join,
        })
    }
}