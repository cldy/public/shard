import { applyDecorators } from '@nestjs/common'

export const UseAccess = () => applyDecorators(addAccessMetadata)

// eslint-disable-next-line @typescript-eslint/ban-types
const addAccessMetadata: ClassDecorator = <T extends Function> (target: T) => {
    Reflect.defineMetadata('useAccess', true, target.prototype)
}