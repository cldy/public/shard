import { applyDecorators } from '@nestjs/common'
import { ManyToMany } from 'typeorm'
import { Role } from '../role.entity'

export const Roles = (): PropertyDecorator => applyDecorators(
    ManyToMany(() => Role, role => role.users) as PropertyDecorator,
)