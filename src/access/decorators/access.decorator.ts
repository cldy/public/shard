import { CustomDecorator, SetMetadata } from '@nestjs/common'
import { Claim } from '../claim.model'


export const Access = (...claims: Claim<any>[]): CustomDecorator => SetMetadata('claims', claims)