import { Controller, UseGuards } from '@nestjs/common'
import { CrudController } from '../common/base/crud.controller'
import { Permission } from './permission.entity'
import { JwtGuard } from '../auth/guards/jwt.guard'
import { PermissionService } from './permission.service'
import { AccessGuard } from './access.guard'

@Controller('permissions')
@UseGuards(JwtGuard, AccessGuard)
export class PermissionController extends CrudController(Permission, PermissionService) {}
