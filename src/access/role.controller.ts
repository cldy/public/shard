import { BadRequestException, Body, Controller, Param, Patch, UseGuards } from '@nestjs/common'
import { CrudController } from '../common/base/crud.controller'
import { Role } from './role.entity'
import { AddUserToRoleCmd } from './commands/addUserToRole.cmd'
import { FindOneUserQuery } from '../user/queries/findOne.user.query'
import { FindOneEntityQuery } from '../common/queries/findOne.entity.query'
import { RoleService } from './role.service'
import { JwtGuard } from '../auth/guards/jwt.guard'
import { Access } from './decorators/access.decorator'
import { Claim } from './claim.model'
import { Action } from './action.enum'
import { AccessGuard } from './access.guard'

@Controller('roles')
@UseGuards(JwtGuard, AccessGuard)
export class RoleController extends CrudController(Role, RoleService) {
    @Patch('/:id/users')
    @Access(new Claim(Action.UPDATE, Role, ['users']))
    async addUserToRole (
        @Param('id') id: string,
        @Body('userId') userId: string,
    ): Promise<void> {
        const user = await this.queryBus.execute(new FindOneUserQuery(userId))
        const role = await this.queryBus.execute(new FindOneEntityQuery(Role, {where: {id}}))

        if (!user || !role) {
            throw new BadRequestException('User or role not found')
        }

        this.commandBus.execute(new AddUserToRoleCmd(user, role))
    }
}