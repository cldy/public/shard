import { BaseService } from '../common/base/base.service'
import { Permission } from './permission.entity'

export class PermissionService extends BaseService(Permission) {}