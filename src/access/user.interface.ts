import { Role } from './role.entity'

export interface IUser {
    id: string
    roles: Role[]
}