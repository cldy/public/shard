import { Column, Entity, JoinTable, ManyToMany, OneToMany } from 'typeorm'
import { BaseModel } from '../common/base/base.model'
import { Permission } from './permission.entity'
import { IUser } from './user.interface'
import { User } from '../user/user.entity'
import { Ability } from '@casl/ability'
import { Claim } from './claim.model'
import { f, forwardRef } from '@marcj/marshal'
import { IsReadOnly } from '../common/validators/isReadOnly.validator'
import { UseAccess } from './decorators/useAccess.decorator'

@UseAccess()
@Entity()
export class Role extends BaseModel {
    @f
    @Column({unique: true})
    name: string

    @f.array(Permission).validator(IsReadOnly)
    @OneToMany(() => Permission, permission => permission.role, {eager: true})
    permissions: Permission[]

    @f.array(forwardRef(() => User)).validator(IsReadOnly)
    @ManyToMany(() => User, user => user.roles)
    @JoinTable()
    users: User[]

    getAbilityForUser (user: IUser) {
        const permissions = this.permissions.map(permission => {
            permission.conditions = permission.getConditions(user)
            return permission
        })
        return new Ability(permissions)
    }

    authorise<T> (user: IUser, claim: Claim<T>) {
        const {action, subject, fields} = claim
        if (fields) {
            return fields.every(field => this.getAbilityForUser(user).can(action, subject, field))
        } else {
            return this.getAbilityForUser(user).can(action, subject)
        }
    }
}