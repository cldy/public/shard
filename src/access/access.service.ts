import { Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { permittedFieldsOf } from '@casl/ability/extra'
import { QueryBus } from '@nestjs/cqrs'
import { FindOneEntityQuery } from '../common/queries/findOne.entity.query'
import { User } from '../user/user.entity'
import { Ability, AbilityTuple } from '@casl/ability'
import { Claim } from './claim.model'
import { Action } from './action.enum'

@Injectable()
export class AccessService {
    constructor (
        private reflector: Reflector,
        private queryBus: QueryBus,
    ) {
    }

    async filter<T extends any> (user: User, data: T, action: Action): Promise<T> {
        if (Array.isArray(data)) {
            const authorisedItems = []
            for (const item of data) {
                await this.authorise(user, new Claim(action, item)) && authorisedItems.push(item)
            }

            return await Promise.all(authorisedItems.map(async item => this.filter(user, item, action))) as T
        }

        if (this.usesAccess(data)) {
            const abilities = await this.getUserAbilities(user)
            const permittedFields = abilities.flatMap(ability => permittedFieldsOf(ability, action, data))

            if (permittedFields.length === 0) {
                return
            }

            Object.keys(data).forEach(key => {
                if (!permittedFields.find(v => v === key)) {
                    delete data[key]
                }
            })
        }

        // Use for loop instead of forEach because forEach doesn't work properly with async functions
        for (const key of Object.keys(data)) {
            if (this.shouldFilter(data[key])) {
                data[key] = await this.filter(user, data[key], action)
            }
        }
        console.log(data, 'after deep filtering')
        return data as T
    }

    async getUserWithRoles (user: User): Promise<User> {
        return user.roles
            ? user
            : await this.queryBus.execute(new FindOneEntityQuery(User, {where: {id: user.id}, relations: ['roles']}))
    }

    async getUserAbilities (user: User): Promise<Ability<AbilityTuple, Record<string, any>>[]> {
        const userWithRoles = await this.getUserWithRoles(user)
        return userWithRoles.roles.map(role => role.getAbilityForUser(userWithRoles))
    }

    async authorise<T extends any> (user: User, claim: Claim<T>): Promise<boolean> {
        if (!this.usesAccess(claim.subject)) {
            return true
        }
        const userWithRoles = await this.getUserWithRoles(user)
        return Boolean(userWithRoles.roles.some(role => role.authorise(userWithRoles, claim)))
    }

    usesAccess (entity: any): boolean {
        return Boolean(this.reflector.get('useAccess', Object.getPrototypeOf(entity)))
    }

    shouldFilter (data: any): boolean {
        return data !== undefined && data !== null && !['string', 'boolean', 'number'].find(v => v === typeof data)
    }
}