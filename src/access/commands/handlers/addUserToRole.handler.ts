import { CommandHandler, ICommandHandler } from '@nestjs/cqrs'
import { AddUserToRoleCmd } from '../addUserToRole.cmd'
import { Repository } from 'typeorm'
import { Role } from '../../role.entity'
import { InjectRepository } from '@nestjs/typeorm'
import { LogService } from '../../../log/log.service'

@CommandHandler(AddUserToRoleCmd)
export class AddUserToRoleHandler implements ICommandHandler<AddUserToRoleCmd> {
    constructor (
        @InjectRepository(Role)
        private readonly roles: Repository<Role>,
        private readonly log: LogService,
    ) {
        this.log.setContext(this.constructor.name)
    }

    async execute (cmd: AddUserToRoleCmd): Promise<void> {
        const {role, user} = cmd

        const roleWithUsers = role.users ? role : await this.roles.findOne(role.id, {relations: ['users']})

        if (roleWithUsers.users.find(v => v === user)) {
            this.log.warn(`User ${user.id} already has the role ${role.id}. No action taken.`)
            return
        }

        roleWithUsers.users.push(user)

        await this.roles.save(roleWithUsers)
    }
}