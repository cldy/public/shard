import { ICommand } from '@nestjs/cqrs'
import { Role } from '../role.entity'
import { User } from '../../user/user.entity'

export class AddUserToRoleCmd implements ICommand {
    constructor (
        public readonly user: User,
        public readonly role: Role,
    ) {
    }
}