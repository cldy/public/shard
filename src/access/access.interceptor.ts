import { ArgumentsHost, CallHandler, Injectable, NestInterceptor } from '@nestjs/common'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { AccessService } from './access.service'
import { Action } from './action.enum'
import { User } from '../user/user.entity'

@Injectable()
export class AccessInterceptor implements NestInterceptor {
    constructor (
        private accessService: AccessService,
    ) {
    }

    async intercept (context: ArgumentsHost, next: CallHandler): Promise<Observable<any>> {
        return next.handle().pipe(map(async data => {
            if (data === null || data === undefined) {
                return data
            }

            const [req] = context.getArgs()
            // TODO: Create built-in Guest role with an abstract Guest user
            const user: User = req.user ?? {id: 'GUEST', roles: []}

            return await this.accessService.filter(user, data, Action.READ)
        }))
    }
}