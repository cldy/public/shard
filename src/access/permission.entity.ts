import { Column, Entity, Index, ManyToOne } from 'typeorm'
import { BaseModel } from '../common/base/base.model'
import { Role } from './role.entity'
import { IUser } from './user.interface'
import { interpolate } from '../common/common.utils'
import { f } from '@marcj/marshal'
import { IsReadOnly } from '../common/validators/isReadOnly.validator'
import { UseAccess } from './decorators/useAccess.decorator'

@UseAccess()
@Entity()
export class Permission extends BaseModel {
    @f
    @Column()
    action: string

    @f
    @Column()
    subject: string

    @f.type(Object).optional()
    @Column({
        type: 'jsonb',
        nullable: true,
    })
    conditions?: Record<string, any>

    @f.array(String).optional()
    @Column('varchar', {
        array: true,
        nullable: true,
    })
    fields?: string[]

    @f
    @Column({
        default: false,
    })
    inverse: boolean

    @f.optional()
    @Column({
        nullable: true,
    })
    reason?: string

    @f.type(Role).validator(IsReadOnly)
    @Index()
    @ManyToOne(() => Role, role => role.permissions)
    role: Role

    @f
    @Column({nullable: true})
    roleId: string

    getConditions (user: IUser) {
        return this.conditions && interpolate(this.conditions, {user: user})
    }
}