import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { Claim } from './claim.model'
import { Reflector } from '@nestjs/core'
import { AccessService } from './access.service'
import { User } from '../user/user.entity'

@Injectable()
export class AccessGuard implements CanActivate {
    constructor (
        private reflector: Reflector,
        private accessService: AccessService,
    ) {
    }

    canActivate (context: ExecutionContext): boolean {
        const req = context.switchToHttp().getRequest()
        const claims: Claim<any>[] = this.reflector.get('claims', context.getHandler())

        if (!claims) {
            return true
        }

        const user: User = req.user

        if (!user) {
            return false
        }

        return claims.every(claim => this.accessService.authorise(user, claim))
    }
}