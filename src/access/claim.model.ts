import { Type } from '@nestjs/common'
import { Action } from './action.enum'

export class Claim<T> {
    constructor (
        public action: Action,
        public subject: T | Type<T> | string,
        public fields?: string[],
    ) {
    }
}