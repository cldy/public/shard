export enum Action {
    READ = 'read',
    LIST = 'list',
    UPDATE = 'update',
    DELETE = 'delete',
    DESTROY = 'destroy',
    CREATE = 'create',
}