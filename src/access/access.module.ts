import { Global, Module } from '@nestjs/common'
import { UserModule } from '../user/user.module'
import { CqrsModule } from '@nestjs/cqrs'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Role } from './role.entity'
import { Permission } from './permission.entity'
import { AccessService } from './access.service'
import { PermissionService } from './permission.service'
import { RoleService } from './role.service'
import { AccessGuard } from './access.guard'
import { AccessInterceptor } from './access.interceptor'
import { RoleController } from './role.controller'
import { PermissionController } from './permission.controller'
import { AddUserToRoleHandler } from './commands/handlers/addUserToRole.handler'
import { APP_INTERCEPTOR } from '@nestjs/core'

@Global()
@Module({
    imports: [
        TypeOrmModule.forFeature([Permission, Role]),
        UserModule,
        CqrsModule,
    ],
    providers: [
        AccessService,
        PermissionService,
        RoleService,
        AccessGuard,
        AccessInterceptor,
        AddUserToRoleHandler,
        {
            provide: APP_INTERCEPTOR,
            useClass: AccessInterceptor,
        },
    ],
    controllers: [
        RoleController,
        PermissionController,
    ],
    exports: [
        AccessInterceptor,
        AccessGuard,
        AccessService,
    ],
})
export class AccessModule {}
