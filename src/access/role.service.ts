import { BaseService } from '../common/base/base.service'
import { Role } from './role.entity'

export class RoleService extends BaseService(Role) {}