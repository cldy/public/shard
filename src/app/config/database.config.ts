import { registerAs } from '@nestjs/config'

export const databaseConfig = registerAs('database', () => ({
    type: process.env.DB_TYPE || 'postgres',
    host: process.env.DB_HOST || 'postgres',
    port: parseInt(process.env.DB_PORT) || 5432,
    username: process.env.DB_USERNAME || 'shard',
    password: process.env.DB_PASSWORD || 'shard',
    database: process.env.DB_DATABASE || 'shard',
    schema: process.env.DB_SCHEMA || 'public',
    synchronize: Boolean(process.env.DB_SYNCHRONIZE) ?? process.env.NODE_ENV === 'development',
    cache: {
        host: process.env.DB_CACHE_HOST || 'redis',
        port: parseInt(process.env.DB_CACHE_PORT) || 6379,
        db: process.env.DB_CACHE_DB || 1,
    },
}))