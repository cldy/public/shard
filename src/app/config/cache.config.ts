import { registerAs } from '@nestjs/config'
import redisStore from 'cache-manager-ioredis'

export const cacheConfig: any = registerAs('cache', () => ({
    store: process.env.CACHE_STORE === 'redis' ? redisStore : 'memory',
    ttl: parseInt(process.env.CACHE_TTL) || 0,
    max: parseInt(process.env.CACHE_MAX) || 100,
    redisHost: process.env.CACHE_REDIS_HOST || 'localhost',
    redisPort: parseInt(process.env.CACHE_REDIS_PORT) || 6379,
    redisPassword: process.env.CACHE_REDIS_PASSWORD || null,
    redisDb: process.env.CACHE_REDIS_DB || 0,
}))