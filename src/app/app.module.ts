import { CacheModule, Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { ConfigModule, ConfigType } from '@nestjs/config'
import { databaseConfig } from './config/database.config'
import { LogModule } from '../log/log.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ConnectionOptions } from 'typeorm'
import { cacheConfig } from './config/cache.config'
import { CommonModule } from '../common/common.module'
import { UserModule } from '../user/user.module'
import { AuthModule } from '../auth/auth.module'
import { AccessModule } from '../access/access.module'

@Module({
    imports: [
        LogModule,
        ConfigModule.forRoot({
            envFilePath: [`${process.env.NODE_ENV || 'development'}.env`, '.env'],
            isGlobal: true,
        }),
        CacheModule.registerAsync({
            useFactory: (config: ConfigType<typeof cacheConfig>) => ({
                store: config.store,
                ttl: 10,
                max: config.max,
                host: config.redisHost,
                port: config.redisPort,
                password: config.redisPassword,
                db: config.redisDb,
            }),
            imports: [ConfigModule.forFeature(cacheConfig)],
            inject: [cacheConfig.KEY],
        }),
        TypeOrmModule.forRootAsync({
            useFactory: (config: ConfigType<typeof databaseConfig>) => ({
                type: config.type,
                host: config.host,
                port: config.port,
                username: config.username,
                password: config.password,
                database: config.database,
                schema: config.schema,
                synchronize: config.synchronize,
                autoLoadEntities: true,
                cache: {
                    type: 'ioredis',
                    options: {
                        host: config.cache.host,
                        port: config.cache.port,
                        db: config.cache.db,
                    },
                },
            } as Partial<ConnectionOptions>),
            imports: [ConfigModule.forFeature(databaseConfig)],
            inject: [databaseConfig.KEY],
        }),
        CommonModule,
        UserModule,
        AuthModule,
        AccessModule,
    ],
    controllers: [AppController],
    providers: [
        AppService,
        // {
        //     provide: APP_INTERCEPTOR,
        //     useClass: CacheInterceptor,
        // },
    ],
})
export class AppModule {}
