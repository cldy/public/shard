import { CommandHandler, ICommandHandler } from '@nestjs/cqrs'
import { LogCmd } from '../log.cmd'
import { LogService } from '../../log.service'

@CommandHandler(LogCmd)
export class LogHandler implements ICommandHandler<LogCmd> {
    constructor (
        private log: LogService,
    ) {
    }

    async execute (command: LogCmd): Promise<void> {
        this.log.log(command.message)
    }
}