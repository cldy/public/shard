// TODO: Implement other functionality of the logger (context, severity, etc.)
import { ICommand } from '@nestjs/cqrs'

export class LogCmd implements ICommand {
    constructor (
        public readonly message: string,
    ) {
    }
}