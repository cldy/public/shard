import { Injectable, Logger, Scope } from '@nestjs/common'

@Injectable({scope: Scope.TRANSIENT}) // Transient scope allows for every instance to have their own context
export class LogService extends Logger {
    constructor (
        context?: string,
    ) {
        super(context, true)
    }
}