import { Global, Module } from '@nestjs/common'
import { LogService } from './log.service'
import { LogSagas } from './log.saga'
import { LogHandler } from './commands/handlers/log.handler'

@Global()
@Module({
    providers: [
        LogService,
        LogHandler,
        LogSagas,
    ],
    exports: [LogService],
})
export class LogModule {
}