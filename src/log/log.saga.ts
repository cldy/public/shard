import { ICommand, ofType, Saga } from '@nestjs/cqrs'
import { Observable } from 'rxjs'
import { LogCmd } from './commands/log.cmd'
import { map } from 'rxjs/operators'
import { Injectable } from '@nestjs/common'
import { EntityCreatedEvent } from '../common/events/entity.created.event'
import { EntityDeletedEvent } from '../common/events/entity.deleted.event'
import { EntityUpdatedEvent } from '../common/events/entity.updated.event'

// TODO: The sagas in this class are for debugging/POC purposes and should eventually be removed
@Injectable()
export class LogSagas {
    @Saga()
    entityCreated = (events$: Observable<any>): Observable<ICommand> => events$.pipe(
        ofType(EntityCreatedEvent),
        map(event => new LogCmd(`Entity created with ID ${event.entity.id}`)),
    )

    @Saga()
    entityUpdated = (events$: Observable<any>): Observable<ICommand> => events$.pipe(
        ofType(EntityUpdatedEvent),
        map(event => new LogCmd(`Entity updated with ID ${event.entity.id}`)),
    )

    @Saga()
    entityDeleted = (events$: Observable<any>): Observable<ICommand> => events$.pipe(
        ofType(EntityDeletedEvent),
        map(event => new LogCmd(`Entity deleted with ID ${event.entity.id}`)),
    )
}